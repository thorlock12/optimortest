Ensure you have version 2 of selenium server standalone, if not download it by:

`wget http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.1.jar1`

And a Java Runtime Environment (can be installed via `sudo apt install default-jre` if not already present)

Then run selenium by:
`java -jar selenium-server-standalone-2.53.1.jar`

Create a python virtualenv and do:

`pip install -r requirements.txt`

Download and extract the latest version of geckodriver:

`wget https://github.com/mozilla/geckodriver/releases/download/v0.13.0/geckodriver-v0.13.0-linux64.tar.gz`

`tar -xvf geckodriver-v0.13.0-linux64.tar.gz`

To run the script:

`python o2_crawler.py`
