import os
import time

from selenium import webdriver
from selenium.common.exceptions import WebDriverException

o2_path = ("http://international.o2.co.uk/internationaltariffs/"
          "calling_abroad_from_uk")
countries = ['Canada', 'Germany', 'Iceland',
             'Pakistan', 'Singapore', 'South Africa']


def select_country(driver, country):
    driver.find_element_by_name('showAllCountries').click()
    driver.find_element_by_link_text(country).click()
    time.sleep(1) # Seems to be dying because page hasn't updated in time?


def get_cost(driver):
    driver.find_element_by_id('paymonthly').click()
    intr_call_card = driver.find_element_by_class_name('intrCallCardbg')
    cost = intr_call_card.find_element_by_xpath('table/tbody/tr[2]/td').text
    rate_of_charge =  intr_call_card.find_element_by_xpath(
        'table/tbody/tr/th[1]'
    ).text.split('\n')[1] # rate of charge could be different...
    return "({}) {}".format(rate_of_charge, cost)


def get_cost_for_country(driver, country):
    select_country(driver, country)
    return "{} {}".format(country, get_cost(driver))


def print_costs_for_countries(driver, countries):
    for country in countries:
        print get_cost_for_country(driver, country)


def start_crawl():
    try:
        # check whether geckodriver is in current working directory
        if 'geckodriver' in os.listdir(os.getcwd()):
            gecko_path = os.path.join(os.getcwd(), 'geckodriver')
            driver = webdriver.Firefox(executable_path=gecko_path)
        # otherwise hope it's correctly set in the system path
        else:
            driver = webdriver.Firefox()
        driver.get(o2_path)
        print_costs_for_countries(driver, countries)
    except WebDriverException:
        print ("Webdriver executable not where expected "
              "please ensure it is located at {} or the system path".format(
                  gecko_path
               ))


if __name__ == '__main__':
    start_crawl()
